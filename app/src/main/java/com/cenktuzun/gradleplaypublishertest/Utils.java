package com.cenktuzun.gradleplaypublishertest;

import android.content.Context;
import android.content.res.Resources;
import android.util.TypedValue;

/**
 * Created by cenktuzun on 22/02/18.
 */

public class Utils {

    public static int convertDpFromSp(Context context, float sp) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, resources.getDisplayMetrics());
    }

    // Code Smells sample
    public static int convertDpFromSpp(Context context, float sp) {
        Resources resources = context.getResources();
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, sp, resources.getDisplayMetrics()) / 2;
    }
    
}
